with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Ada.Sequential_IO;
with Ada.Real_Time;
with Interfaces;
with Interfaces.C;

with GLUS;
with GL_H;

use Ada.Text_IO;
use Ada.Integer_Text_IO;
use Interfaces;
use Ada.Real_Time;
use Interfaces.C;
use GLUS;
use GL_H;

package body main is

  ------------------------------------------------------------------------
  ----
  g_width  : constant Integer := 800;
  g_height : constant Integer := 600;

  type Pixel is record
    r,g,b,a : Unsigned_8;
  end record;

  type ScreenBuffer is array (0 .. g_width*g_height - 1) of Pixel;

  g_fbdata : ScreenBuffer;

  procedure Put_Pixel(x : Integer; y : Integer; pix : Pixel) is
  begin
    g_fbdata((g_height-y-1)*g_width + x) := pix;
  end Put_Pixel;

  function Get_Pixel(x : Integer; y : Integer) return Pixel is
  begin
    return g_fbdata((g_height-y-1)*g_width + x);
  end Get_Pixel;
  ----
  ------------------------------------------------------------------------


  --
  --
  function On_Create_Window return GLUSboolean is
  begin
    return GLUS_TRUE;
  end On_Create_Window;


  procedure On_Destroy_Window is
  begin
    null;
  end On_Destroy_Window;


  procedure Keyboard(pressed : GLUSboolean; key : Integer) is
  begin
    null;
  end Keyboard;

  procedure Mouse(pressed : GLUSboolean; button : Integer; x : Integer; y : Integer) is
  begin

    if integer(pressed) = 0 then
      return;
    end if;

    if (Unsigned_32(button) and Unsigned_32(1)) > 0 then
      Put("left button pressed at :");
      Put(Integer'Image(x));
      Put(" ");
      Put_Line(Integer'Image(y));
      Put_Pixel(x, y, pix => (255,255,0,0));
    end if;

    if (Unsigned_32(button) and Unsigned_32(4)) > 0 then
      Put("right button pressed at :");
      Put(Integer'Image(x));
      Put(" ");
      Put_Line(Integer'Image(y));
      Put_Pixel(x, y, pix => (255,0,255,0));
    end if;


  end Mouse;

  procedure Mouse_Move(button : Integer; x : Integer; y : Integer) is
  begin
    null;
  end Mouse_Move;

  --
  --
  procedure Reshape(width : Integer; height : Integer) is
  begin
    glViewport(0, 0, width, height);
  end Reshape;

  --
  --
  function Render_Frame(a_deltaTime : float) return GLUSboolean is
  begin

    glClearColor(0.0, 0.0, 1.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT or GL_STENCIL_BUFFER_BIT);

    glDrawPixels(g_width, g_height, GL_RGBA, GL_UNSIGNED_BYTE, g_fbdata(0)'Address);

    return GLUS_TRUE;

  end Render_Frame;



  ---- this is an example of working with frame buffer
  --
  procedure Fill_Screen(pix : Pixel) is
  begin

    for y in 0 .. g_height-1 loop
      for x in 0 .. g_width-1 loop
        Put_Pixel(x,y,pix);
      end loop;
    end loop;

  end Fill_Screen;


  -- entry point
  --
  procedure Main is

  begin

    Put_Line("start program");

    Fill_Screen(pix => (0,0,20,0));

    Put_Line("cleared screen");

    cpp_create_context_and_window(1, 0, g_width, g_height);

    cpp_main_loop;

    Put_Line("end program");

  end Main;


end main;
